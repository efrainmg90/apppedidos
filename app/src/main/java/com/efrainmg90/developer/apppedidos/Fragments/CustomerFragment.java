package com.efrainmg90.developer.apppedidos.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.efrainmg90.developer.apppedidos.Activities.MapsActivity;
import com.efrainmg90.developer.apppedidos.Models.Customer;
import com.efrainmg90.developer.apppedidos.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CustomerFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    RequestQueue requestQueue;
    private Gson gson;
    private ListView listViewCustomers;
    List<String> customerNameList;
    ArrayAdapter<String> arrayAdapter;
    FloatingActionButton fab;
    EditText nameCustomer, emailCustomer,latitudeCustomer,longitudeCustomer;
    AppCompatButton buttonGetLoc;
    TextView locations;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public CustomerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CustomerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CustomerFragment newInstance(String param1, String param2) {
        CustomerFragment fragment = new CustomerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestQueue = Volley.newRequestQueue(getActivity());
        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                loadForm();
            }
        });
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_customer, container, false);
        listViewCustomers = (ListView) view.findViewById(R.id.list_view_customers);
        getCustomers();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void getCustomers(){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        String URL = getString(R.string.url)+"customers";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        List<Customer> customerList = Arrays.asList(gson.fromJson(String.valueOf(response),Customer[].class));
                        loadListView(customerList);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        //add request to queue
        requestQueue.add(jsonArrayRequest);
    }// End getCustomers

    private void loadListView(List<Customer> customerList){
        customerNameList = new ArrayList<String>();
        for(Customer customer:customerList){
            customerNameList.add(customer.getName());
            Log.i("Product:",customer.getName());
        }
        arrayAdapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,customerNameList);
        listViewCustomers.setAdapter(arrayAdapter);
    }

    private void loadForm() {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.form_customer, null);
        nameCustomer = (EditText) view.findViewById(R.id.input_name_customer);
        emailCustomer = (EditText) view.findViewById(R.id.input_email_customer);
        latitudeCustomer = (EditText) view.findViewById(R.id.input_latitude_customer);
        longitudeCustomer = (EditText) view.findViewById(R.id.input_longitude_customer);
        buttonGetLoc = (AppCompatButton) view.findViewById(R.id.btn_customer_get_loc);
        locations = (TextView) view.findViewById(R.id.localitation_form_customer);
        buttonGetLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MapsActivity.class);
                startActivityForResult(intent, 1);
            }
        });
        //Building dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Customer customer = new Customer();
                customer.setName(nameCustomer.getText().toString());
                customer.setEmail(emailCustomer.getText().toString());
                customer.setLatitude(latitudeCustomer.getText().toString());
                customer.setLongitude(longitudeCustomer.getText().toString());
                Log.e("Customer a guardar:",customer.toString());
                addCustomer(customer);
                dialog.dismiss();
                //save info where you want it
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }//loadForm

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                double latitude = data.getDoubleExtra("latitude",0.0);
                double longitude = data.getDoubleExtra("longitude",0.0);
                if(locations!=null){
                    locations.setText("La direccion ha sido agregada:");
                    latitudeCustomer.setText(String.valueOf(latitude));
                    longitudeCustomer.setText(String.valueOf(longitude ));
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
    private void addCustomer(final Customer customer){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        String URL = getString(R.string.url)+"customers";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name", customer.getName());
        params.put("email", customer.getEmail());
        params.put("latitude", customer.getLatitude());
        params.put("longitude", customer.getLongitude());
        JSONObject jsonParams = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            String message = response.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            customerNameList.add(customer.getName());
                            arrayAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), "Error de request", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error de conexion al servidor", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }// End addproducts
}

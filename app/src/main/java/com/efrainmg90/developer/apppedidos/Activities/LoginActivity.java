package com.efrainmg90.developer.apppedidos.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.efrainmg90.developer.apppedidos.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    AppCompatButton btnLogin;
    EditText email,password;
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        boostrap();
    }
    private void boostrap(){
        email = (EditText) findViewById(R.id.input_email);
        password = (EditText) findViewById(R.id.input_password);
        btnLogin = (AppCompatButton) findViewById(R.id.btn_login);
        requestQueue = Volley.newRequestQueue(this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailStr=email.getText().toString();
                String passwordStr=password.getText().toString();
               if(validateInputs(emailStr,passwordStr)){
                   login(emailStr,passwordStr);
               }
            }
        });
    }//end bootstrap

    private boolean validateInputs(String email,String password){
        email = email.trim();
        password = password.trim();
        if(!email.equals("") && !password.equals("")){
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (!email.matches(emailPattern))
                {
                    Toast.makeText(getApplicationContext(),"Correo invalido",Toast.LENGTH_SHORT).show();
                    return false;
                }else{
                    return true;
                }
        } else
        {
            Toast.makeText(getApplicationContext(),"Correo o contraseña invalida", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private void login(final String email, String password){
        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Iniciando Sesion...");
        progressDialog.show();
        String URL = getString(R.string.url)+"signin";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        params.put("password", password);
        JSONObject jsonParams = new JSONObject(params);

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonParams, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                try {
                    String token = response.getString("token");
                    String username = response.getString("user");
                    //Toast.makeText(LoginActivity.this, "El token es: "+token, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("name", username);
                    bundle.putString("email", email);
                    bundle.putString("token", token);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                } catch (JSONException e) {
                    Toast.makeText(LoginActivity.this, "Error de usuario y contraseña", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, "Error de conexion al servidor", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };
        requestQueue.add(postRequest);
    }//end login

}

package com.efrainmg90.developer.apppedidos.Fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.efrainmg90.developer.apppedidos.Activities.RouteMapsActivity;
import com.efrainmg90.developer.apppedidos.Models.Order;
import com.efrainmg90.developer.apppedidos.Models.OrderDetails;
import com.efrainmg90.developer.apppedidos.Models.Product;
import com.efrainmg90.developer.apppedidos.Models.Route;
import com.efrainmg90.developer.apppedidos.Models.RouteCustomer;
import com.efrainmg90.developer.apppedidos.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RouteFragment extends Fragment {

    RequestQueue requestQueue;
    private Gson gson;
    private ListView listViewRoute;
    List<String> routeNameList;
    List<Route> routeList;
    String token;
    ArrayAdapter<String> arrayAdapter;
    FloatingActionButton fab;
    EditText nameRoute;

    public RouteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestQueue = Volley.newRequestQueue(getActivity());
        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                loadForm();
            }
        });
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("M/d/yy hh:mm a");
        gson = gsonBuilder.create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        token = getArguments().getString("token");
        View view = inflater.inflate(R.layout.fragment_route, container, false);
        listViewRoute = (ListView) view.findViewById(R.id.list_view_routes);
        getRoutes();
        listViewRoute.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                return false;
            }
        });
        listViewRoute.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                getOrdersDetails(i);
            }
        });
        return view;
    }

    private void getRoutes(){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        String URL = getString(R.string.url)+"routes";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        routeList = Arrays.asList(gson.fromJson(String.valueOf(response),Route[].class));
                        loadListView(routeList);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        //add request to queue
        requestQueue.add(jsonArrayRequest);
    }// End products

    private void loadListView(List<Route> routeList){
        routeNameList = new ArrayList<String>();
        for(Route route:routeList){
            routeNameList.add(route.getName());
            Log.i("Route:",route.getName());
        }
        arrayAdapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,routeNameList);
        listViewRoute.setAdapter(arrayAdapter);
    }

    private void loadForm(){
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.form_route,null);
        final EditText editTextName = (EditText) view.findViewById(R.id.input_name_route);
        //Building dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                addRoute(editTextName.getText().toString());
                dialog.dismiss();
                //save info where you want it
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }//loadForm

    private void addRoute(final String name){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        String URL = getString(R.string.url)+"routes";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name", name);
        JSONObject jsonParams = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            String message = response.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            routeNameList.add(name);
                            arrayAdapter.notifyDataSetChanged();
                            getRoutes();

                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), "Error de request", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error de conexion al servidor", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }// End addRoute

    private void getOrdersDetails(int position){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        Route route = routeList.get(position);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        String URL = getString(R.string.url)+"routes/"+route.getId()+"?token="+token;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        List<RouteCustomer> routeCustomerList = Arrays.asList(gson.fromJson(String.valueOf(response),RouteCustomer[].class));
                        Intent intent = new Intent(getActivity(), RouteMapsActivity.class);
                        intent.putExtra("routeList", (Serializable) routeCustomerList);
                        startActivity(intent);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error de peticion", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };
        //add request to queue
        requestQueue.add(jsonArrayRequest);
    }// End products

    private void loadDetails(List<OrderDetails> OrDetlist){
        List<String> orderDetailsName = new ArrayList<String>();
        for(OrderDetails details:OrDetlist){
            orderDetailsName.add(details.getQuantity()+" piezas de "+details.getProduct());
        }
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.layout_order_details,null);
        ListView listView = (ListView) view.findViewById(R.id.list_view_orders_details);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,orderDetailsName);
        listView.setAdapter(arrayAdapter);
        //Building dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //save info where you want it
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }//loadFormdetails
}

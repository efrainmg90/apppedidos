package com.efrainmg90.developer.apppedidos.Models;

import java.util.Date;

/**
 * Created by Developer on 04/06/2017.
 */

public class Order {
    long id;
    String customer;
    String date;

    public Order() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", customer='" + customer + '\'' +
                ", date=" + date +
                '}';
    }
}

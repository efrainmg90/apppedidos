package com.efrainmg90.developer.apppedidos.Models;

import java.io.Serializable;

/**
 * Created by Developer on 15/06/2017.
 */

public class RouteCustomer implements Serializable{
    long id;
    String customer;
    String latitude;
    String longitude;
    long route_id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return customer;
    }

    public void setName(String customer) {
        this.customer = customer;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public long getRoute_id() {
        return route_id;
    }

    public void setRoute_id(long route_id) {
        this.route_id = route_id;
    }

    @Override
    public String toString() {
        return "RouteCustomer{" +
                "id=" + id +
                ", name='" + customer + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", route_id=" + route_id +
                '}';
    }
}

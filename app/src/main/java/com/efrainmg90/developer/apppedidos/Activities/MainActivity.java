package com.efrainmg90.developer.apppedidos.Activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.efrainmg90.developer.apppedidos.Fragments.CustomerFragment;
import com.efrainmg90.developer.apppedidos.Fragments.HomeFragment;
import com.efrainmg90.developer.apppedidos.Fragments.OrderFragment;
import com.efrainmg90.developer.apppedidos.Fragments.ProductFragment;
import com.efrainmg90.developer.apppedidos.Fragments.RouteFragment;
import com.efrainmg90.developer.apppedidos.R;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    String token;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);

        //my variables
        Bundle bundle = getIntent().getExtras();
        token = bundle.getString("token");
        //Toast.makeText(this, token, Toast.LENGTH_SHORT).show();

        TextView username = (TextView) headerView.findViewById(R.id.username);
        TextView emailUser = (TextView) headerView.findViewById(R.id.email_user);
        username.setText(bundle.getString("name"));
        emailUser.setText(bundle.getString("email"));

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_content, new HomeFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fragmentManager = getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putString("token", token);

        if (id == R.id.nav_routes) {
            fab.show();
            RouteFragment routeFragment = new RouteFragment();
            routeFragment.setArguments(bundle);
            fragmentManager.beginTransaction().replace(R.id.fragment_content, routeFragment).commit();
        } else if (id == R.id.nav_orders) {
            fab.show();
            OrderFragment orderFragment = new OrderFragment();
            orderFragment.setArguments(bundle);
            fragmentManager.beginTransaction().replace(R.id.fragment_content, orderFragment).commit();
        } else if (id == R.id.nav_customers) {
            fab.show();
            fragmentManager.beginTransaction().replace(R.id.fragment_content, new CustomerFragment()).commit();

        } else if (id == R.id.nav_products) {
            fab.show();
            fragmentManager.beginTransaction().replace(R.id.fragment_content, new ProductFragment()).commit();
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

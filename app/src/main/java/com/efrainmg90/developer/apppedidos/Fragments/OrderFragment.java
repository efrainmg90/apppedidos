package com.efrainmg90.developer.apppedidos.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.efrainmg90.developer.apppedidos.Models.Customer;
import com.efrainmg90.developer.apppedidos.Models.Order;
import com.efrainmg90.developer.apppedidos.Models.OrderDetails;
import com.efrainmg90.developer.apppedidos.Models.Product;
import com.efrainmg90.developer.apppedidos.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import static android.content.Context.VIBRATOR_SERVICE;


public class OrderFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    RequestQueue requestQueue;
    private Gson gson;
    private ListView listViewOrders;
    List<String> orderNameList,productNameList,customerNameList;
    List<Product> productList;
    List<Order> orderList;
    Hashtable<Long,String> productAsoc,customerAsoc;
    List<Customer> customerList;
    ArrayAdapter<String> arrayAdapter;
    FloatingActionButton fab;
    EditText quantity;
    Boolean createOrEdit= false;
    Spinner spinnerCustomer, spinnerProduct;
    String token;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public OrderFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestQueue = Volley.newRequestQueue(getActivity());
        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                loadForm();
            }
        });
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("yyyy-MM-dd hh:mm:ss.S");
        gson = gsonBuilder.create();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        token = getArguments().getString("token");
        //Toast.makeText(getActivity(), token, Toast.LENGTH_SHORT).show();
        View view = inflater.inflate(R.layout.fragment_order, container, false);
        listViewOrders = (ListView) view.findViewById(R.id.list_view_orders);
        getOrders();
        listViewOrders.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                loadOptionsForListView(position);
                return false;
            }
        });
        listViewOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getOrdersDetails(position);
            }
        });
        return view;
    }

    private void getOrders(){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        String URL = getString(R.string.url)+"orders?token="+token;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        orderList = Arrays.asList(gson.fromJson(String.valueOf(response),Order[].class));
                        loadListView(orderList);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error de peticion", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                headers.put("Authorization", "Bearer "+token);
                return headers;
            }
        };
        //add request to queue
        requestQueue.add(jsonArrayRequest);
    }// End getOrders
    private void loadListView(List<Order> orderList){
        orderNameList = new ArrayList<String>();
        for(Order order:orderList){
            orderNameList.add("Pedido: "+order.getId()+" -- Cliente:"+order.getCustomer());
            Log.i("Product:","Pedido: "+order.getId()+" -- Cliente:"+order.getCustomer());
        }
        arrayAdapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,orderNameList);
        listViewOrders.setAdapter(arrayAdapter);
    }

    private void loadForm(){
        createOrEdit = false;
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.form_order,null);
        spinnerCustomer = (Spinner) view.findViewById(R.id.spinner_customer_id_order);
        getCustomers();
        spinnerProduct = (Spinner) view.findViewById(R.id.spinner_product_id_order);
        getProducts();
        quantity = (EditText) view.findViewById(R.id.input_quantity_order);
        //Building dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Customer customerAdd = customerList.get(spinnerCustomer.getSelectedItemPosition());
                Product productAdd = productList.get(spinnerProduct.getSelectedItemPosition());
                String quantityAdd = quantity.getText().toString();
                //Toast.makeText(getActivity(), "Customer:"+customerAdd.getName()+" Producto:"+productAdd.getName()+" Cant:"+quantityAdd, Toast.LENGTH_SHORT).show();
                addOrder(customerAdd.getId(),productAdd.getId(),quantityAdd,customerAdd.getName());
                Log.e("AntesGuardar","Customer:"+customerAdd.getName()+" Producto:"+productAdd.getName()+" Cant:"+quantityAdd);
                dialog.dismiss();
                //save info where you want it
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }//loadForm

    private void getProducts(){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        String URL = getString(R.string.url)+"products?token="+token;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                         productList = Arrays.asList(gson.fromJson(String.valueOf(response),Product[].class));
                        loadSpinnerProduct(productList);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        //add request to queue
        requestQueue.add(jsonArrayRequest);
    }// End products

    private void loadSpinnerProduct(List<Product> productList){
        productNameList = new ArrayList<String>();
        productAsoc = new Hashtable<Long,String>();
        for(Product product:productList){
            productAsoc.put(product.getId(),product.getName());
            productNameList.add(product.getName());
            Log.i("Product:",product.getName());
        }
        spinnerProduct.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, productNameList));
    }
    private void getCustomers(){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        String URL = getString(R.string.url)+"customers?token="+token;

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        customerList = Arrays.asList(gson.fromJson(String.valueOf(response),Customer[].class));
                        loadSpinnerCustomers(customerList);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                });
        //add request to queue
        requestQueue.add(jsonArrayRequest);
    }// End getCustomers

    private void loadSpinnerCustomers(List<Customer> customerList){
        customerNameList = new ArrayList<String>();
        customerAsoc = new Hashtable<Long,String>();
        for(Customer customer:customerList){
            customerNameList.add(customer.getName());
            customerAsoc.put(customer.getId(),customer.getName());
            Log.i("Product:",customer.getName());
        }
        spinnerCustomer.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, customerNameList));
        if(createOrEdit){}
            //spinnerCustomer.setSelection(customerNameList.indexOf(orderUpdate.getCustomer()));
    }

    private void addOrder(long customer_id, final long product_id, final String quantity, final String nameCustomer){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        String URL = getString(R.string.url)+"orders?token="+token;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("customer_id", String.valueOf(customer_id));
        JSONObject jsonParams = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("addOrder","guardado");
                        progressDialog.dismiss();
                        try {
                            long id = response.getLong("id");
                            Log.e("add detail","id:"+id+" prodId"+product_id+" qty:"+quantity);
                            //Toast.makeText(getActivity(),"id:"+id+" prodId"+product_id+" qty:"+quantity, Toast.LENGTH_SHORT).show();
                            addOrderDetail(id,product_id,quantity);
                            //Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            orderNameList.add("Pedido: "+id+" -- Cliente:"+nameCustomer);
                            arrayAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), "Error de request", Toast.LENGTH_SHORT).show();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error de conexion al servidor", Toast.LENGTH_SHORT).show();
                        Log.e("Nomames si si se guardo","id:"+"?"+" prodId"+product_id+" qty:"+quantity);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                headers.put("Authorization", "Bearer "+token);
                return headers;
            }
        };
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }// End addOrder

    private void addOrderDetail(final long order_id, long product_id, String quantity){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        String URL = getString(R.string.url)+"orders/"+order_id+"?token="+token;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("product_id", String.valueOf(product_id));
        params.put("quantity", quantity);
        JSONObject jsonParams = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            String message = response.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            loadFormDetails(order_id);

                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), "Error de request", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.e("Error: ", error.getMessage());
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error de conexion al servidor", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                headers.put("Authorization", "Bearer "+token);
                return headers;
            }
        };
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }// End addOrder

    private void loadFormDetails(final long order_id){
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.form_order_detail,null);
        spinnerProduct = (Spinner) view.findViewById(R.id.spinner_product_id_order_detail);
        spinnerProduct.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, productNameList));
        quantity = (EditText) view.findViewById(R.id.input_quantity_order_detail);
        //Building dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Product productAdd = productList.get(spinnerProduct.getSelectedItemPosition());
                String quantityAdd = quantity.getText().toString();
                //Toast.makeText(getActivity(), "Customer:"+customerAdd.getName()+" Producto:"+productAdd.getName()+" Cant:"+quantityAdd, Toast.LENGTH_SHORT).show();
                addOrderDetail(order_id,productAdd.getId(),quantityAdd);
                Log.e("Add Detail"," Producto:"+productAdd.getName()+" Cant:"+quantityAdd);
                dialog.dismiss();
                //save info where you want it
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getOrders();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }//loadFormdetails

    private void loadOptionsForListView( final int position){
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.layout_options_listview,null);
        Button btnOpEdit = (Button) view.findViewById(R.id.button_edit);
        Button btnOpDelete = (Button) view.findViewById(R.id.button_delete);
        //Building dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("¿Que operación desea realizar?: ");
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        Vibrator vibrate = (Vibrator) getActivity().getSystemService(VIBRATOR_SERVICE);
        vibrate.vibrate(200);
        btnOpEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editOption(position);
                dialog.dismiss();
            }
        });
        btnOpDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //deleteOption
                deleteOrder(position);
                dialog.dismiss();
            }
        });

        dialog.show();
    }//end showOptions listview

    private void editOption( final int position){
        createOrEdit = true;
        final Order orderUpdate = orderList.get(position);
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.form_order_edit,null);
        spinnerCustomer = (Spinner) view.findViewById(R.id.spinner_customer_id_order_edit);
        getCustomers();
//        spinnerCustomer.setSelection(customerNameList.indexOf(orderUpdate.getCustomer()));
        //Building dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Customer customerUpd = customerList.get(spinnerCustomer.getSelectedItemPosition());
                updateOrder(customerUpd.getId(),orderUpdate.getId(),position);//cambiar a edit
                dialog.dismiss();
                //save info where you want it
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    } //end Load form edit

    private void updateOrder(final long customer_id, long id, final int position){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        String URL = getString(R.string.url)+"orders/"+id+"?token="+token;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("customer_id", String.valueOf(customer_id));
        JSONObject jsonParams = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, URL, jsonParams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            String message = response.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            //productNameList.set(position,name);
                            //arrayAdapter.notifyDataSetChanged();
                            getOrders();
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), "Error de request", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error de conexion al servidor", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                headers.put("Authorization", "Bearer "+token);
                return headers;
            }
        };
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }// End updateOrder
    private void deleteOrder(final int position){
        final Order orderInDB = orderList.get(position);
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        String URL = getString(R.string.url)+"orders/"+orderInDB.getId()+"?token="+token;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.DELETE, URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            String message = response.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            //productNameList.remove(position);
                            //arrayAdapter.notifyDataSetChanged();
                            getOrders();
                        } catch (JSONException e) {
                            Toast.makeText(getActivity(), "Error de request", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error de conexion al servidor", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                headers.put("Authorization", "Bearer "+token);
                return headers;
            }
        };
        //add request to queue
        requestQueue.add(jsonObjectRequest);
    }// End deleteOrder

    private void loadDetails(List<OrderDetails> OrDetlist){
        List<String> orderDetailsName = new ArrayList<String>();
        for(OrderDetails details:OrDetlist){
            orderDetailsName.add(details.getQuantity()+" piezas de "+details.getProduct());
        }
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View view = layoutInflater.inflate(R.layout.layout_order_details,null);
        ListView listView = (ListView) view.findViewById(R.id.list_view_orders_details);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,orderDetailsName);
        listView.setAdapter(arrayAdapter);
        //Building dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                //save info where you want it
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }//loadFormdetails

    private void getOrdersDetails(int position){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        Order order = orderList.get(position);
        progressDialog.setMessage("Cargando...");
        progressDialog.show();
        String URL = getString(R.string.url)+"orders/"+order.getId()+"?token="+token;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressDialog.dismiss();
                        List<OrderDetails> orderDetailsList = Arrays.asList(gson.fromJson(String.valueOf(response),OrderDetails[].class));
                        loadDetails(orderDetailsList);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getActivity(), "Error de peticion", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                headers.put("Authorization", "Bearer "+token);
                return headers;
            }
        };
        //add request to queue
        requestQueue.add(jsonArrayRequest);
    }// End products
}

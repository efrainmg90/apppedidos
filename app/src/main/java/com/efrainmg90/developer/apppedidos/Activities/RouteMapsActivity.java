package com.efrainmg90.developer.apppedidos.Activities;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.efrainmg90.developer.apppedidos.Models.RouteCustomer;
import com.efrainmg90.developer.apppedidos.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class RouteMapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ArrayList<RouteCustomer> routeCustomerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_maps);
        routeCustomerList = (ArrayList<RouteCustomer>) getIntent().getSerializableExtra("routeList");
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        for(RouteCustomer routeCustomer:routeCustomerList){
            addMark(routeCustomer);
        }
        // Add a marker in Sydney and move the camera
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    private void addMark(RouteCustomer routeCustomer) {
        LatLng coodinates = new LatLng(Double.parseDouble(routeCustomer.getLatitude()) , Double.parseDouble(routeCustomer.getLongitude()));
        CameraUpdate mylocation = CameraUpdateFactory.newLatLngZoom(coodinates, 12);
       Marker marker = mMap.addMarker(new MarkerOptions()
                .position(coodinates).title(routeCustomer.getName()));
        mMap.animateCamera(mylocation);
    }//end addMark
}

package com.efrainmg90.developer.apppedidos.Models;

/**
 * Created by Developer on 10/06/2017.
 */

public class Route {
    long id;
    String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
